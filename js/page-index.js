"use strict";


var closeFiltersPanel = function (e) {
    $(".filter-wrap").css('display','none');
    $('.overlay').slideToggle('fast', function() {
        $('.overlay').css('display','none');
    });
    e.stopPropagation();
    $("html,body").css("overflow","auto");
};

var toogleSidebarUl = function (e, context) {
    e.stopPropagation();
    if($(context).parent().has("ul")) {
        if(!$(context).hasClass("open")) {
            // hide any open menus and remove all other classes
            $(".ul-wrap li ul").slideUp(350);
            $(".ul-wrap li a").removeClass("open");

            // open our new menu and add the open class
            $(context).next("ul").slideDown(350);
            $(context).addClass("open");
        }
        else {
            $(context).removeClass("open");
            $(context).next("ul").slideUp(350);
        }
    }
};

$(document).ready(function(e){



    $(".worker-wrap").mouseenter(function(){
        $(".worker-wrap").animate({"left": "+=100px"}, "slow");
    });
    $(".worker-wrap").mouseleave(function(){
        $(".worker-wrap").animate({"left": "-=100px"}, "slow");
    });

    /*sidebar->items-> toogle sub-items*/
    $(".sidebar .menu-wrap .ul-wrap > li > a").on("click", function(e){
        toogleSidebarUl(e, this);
    });

    /*sidebar->items-> toogle sub-items*/
    // $(".sidebar .menu-wrap .ul-wrap > li > a").on("mouseenter", function(e){
    //     if($( window ).width()>992){
    //         toogleSidebarUl(e, this);
    //     }
    // });

    /* toogle sidebar when resize window*/
    $( window ).resize(function() {
        if($( window ).width()>992){
            $("main .sidebar").css('display','inline-block');
        }else {
            $("main .sidebar").css('display','none');
        }
    });
    $(window).on("click", function(e) {
        if($("header .navbar-header>.navbar-toggle")[0].nodeName != e.target.nodeName){
            if($( window ).width()<992) {
                $("main .sidebar").css('display', 'none');
                e.stopPropagation();
            }
        }
    });

    $("header .navbar-header>.navbar-toggle").on("click", function(e){
        $(".sidebar").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','inline-block');
            }
        });
        e.stopPropagation();
    });

    /* click filters toogle*/
    $("main .main-content .section>.section-header>.filters-wrap>.filter-button-wrap>a").on("click ", function(e){
        $(".filter-wrap").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','block');
            }
        });
        $(".overlay").slideToggle('fast', function() {
            $('.overlay').css('display','block');
        });
        e.stopPropagation();
        $("html,body").css("overflow","hidden");
        //   $("main .filter-wrap").css("overflow","auto");

    });
    $(".filter-wrap>.filter-header .button-close").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main  .overlay").on("click touchstart", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .reset-filters").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .set-filters").on("click", function(e){
        closeFiltersPanel(e);
    });
});



ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10
    });
    // var     myPlacemark1 = new ymaps.Placemark([55.8, 37.6], {
    //         iconContent: '',
    //         balloonContent: '<div class="product" style="display: block; margin:-11px; padding:5px; width:132px;"><a href="#" style="display: block;text-align: left;text-decoration: none;background: #FFF;border: 1px solid #E8ECF2; width:100%;"><div class="product-img" style="position: relative; border-bottom: 1px solid #E8ECF2; background: ivory;height:130px;width:100%;"><div class="frame-square" style="background: #fff;width: 100%;height: 100%; margin-right: .5em; margin-bottom: .3em;"><div class="crop" style="height: 100%; overflow: hidden;position: relative;"><img src="https://dl.dropboxusercontent.com/u/33476038/public_images/_MG_7369.jpg" alt="" title="Name" style=" display: block; min-width: 100%; min-height: 100%; margin: auto; position: absolute; top: -100%; right: -100%;bottom: -100%;left: -100%;"></div></div></div><p class="address text-nowrap" style="position: relative; margin: -51px 0 -4px 0;padding: 0; font-size: 14px;line-height: 63px;font-weight: 100;color:#ffffff;background: linear-gradient(0deg, rgba(0,0,0, 0.5), rgba(0,0,0, 0.0)); width: 100%; height: 50px;border: none;"><span style="padding-left: 10px;">Address</span></p><p class="price text-nowrap" style="font-weight: 600;font-size: 14px; color: #2288cc; width: 100%; margin: 10px 5px 5px 10px;"><span style="">4 500</span><span style="">₽</span></p><p class="name text-nowrap" style="font-weight: 300; font-size: 12px; color: #666;width: 100%;margin: 5px 5px 10px 10px;">Name</p></a></div>',
    //         hintContent: 'Категория'
    //     }, {
    //         preset: 'twirl#violetIcon'
    //     }),
    //
    //     myPlacemark2 = new ymaps.Placemark([55.82, 37.62], {
    //         iconContent: '',
    //         balloonContent: '<div class="product" style="' +
    //                                     'display: block; ' +
    //                                     'margin:-11px; ' +
    //                                     'padding:5px; ' +
    //                                     'width:132px;">' +
    //                             '<a href="#" style="' +
    //                             'display: block;' +
    //                             'text-align: left;' +
    //                             'text-decoration: none;' +
    //                             'background: #FFF;' +
    //                             'border: 1px solid #E8ECF2; ' +
    //                             'width:100%;">' +
    //                                 '<div class="product-img" style="' +
    //                                 'position: relative; ' +
    //                                 'border-bottom: 1px solid #E8ECF2; ' +
    //                                 'background: ivory;' +
    //                                 'height:130px;' +
    //                                 'width:100%;">' +
    //                                     '<div class="frame-square" style="' +
    //                                     'background: #fff;' +
    //                                     'width: 100%;' +
    //                                     'height: 100%; ' +
    //                                     'margin-right: .5em; ' +
    //                                     'margin-bottom: .3em;">' +
    //                                         '<div class="crop" style="' +
    //                                         'height: 100%; ' +
    //                                         'overflow: hidden;' +
    //                                         'position: relative;">' +
    //                                             '<img src="https://dl.dropboxusercontent.com/u/33476038/public_images/_MG_7369.jpg" alt="" title="Name" style="' +
    //                                             'display: block; ' +
    //                                             'min-width: 100%; ' +
    //                                             'min-height: 100%;' +
    //                                             'margin: auto; ' +
    //                                             'position: absolute; ' +
    //                                             'top: -100%; ' +
    //                                             'right: -100%;' +
    //                                             'bottom: -100%;' +
    //                                             'left: -100%;">' +
    //                                     '</div>' +
    //                                     '</div>' +
    //                                 '</div>' +
    //                                 '<p class="address text-nowrap" style="' +
    //                                     'position: relative; ' +
    //                                     'margin: -51px 0 -4px 0;' +
    //                                     'padding: 0; ' +
    //                                     'font-size: 14px;' +
    //                                     'line-height: 63px;' +
    //                                     'font-weight: 100;' +
    //                                     'color:#ffffff;' +
    //                                     'background: linear-gradient(0deg, rgba(0,0,0, 0.5), rgba(0,0,0, 0.0)); ' +
    //                                     'width: 100%; ' +
    //                                     'height: 50px;' +
    //                                     'border: none;">' +
    //                                         '<span style="padding-left: 10px;">Address</span>' +
    //                                 '</p>' +
    //                             '<p class="price text-nowrap" style="' +
    //                                 'font-weight: 600;' +
    //                                 'font-size: 14px; ' +
    //                                 'color: #2288cc; ' +
    //                                 'width: 100%; ' +
    //                                 'margin: 10px 5px 5px 10px;">' +
    //                                     '<span style="">4 500</span>' +
    //                                     '<span style="">₽</span>' +
    //                             '</p>' +
    //                             '<p class="name text-nowrap" style="' +
    //                                 'font-weight: 300; ' +
    //                                 'font-size: 12px; ' +
    //                                 'color: #666;' +
    //                                 'width: 100%;' +
    //                                 'margin: 5px 5px 10px 10px;">' +
    //                                 'Name' +
    //                             '</p>' +
    //             '</a>' +
    //         '</div>',
    //         hintContent: 'Категория'
    //     }, {
    //         preset: 'twirl#violetIcon'
    //     });
    // myMap.geoObjects
    //     .add(myPlacemark1)
    //     .add(myPlacemark2);

    myMap.controls
        .add('zoomControl', { left: 5, top: 5 })
       // .add('smallZoomControl')
        .add('typeSelector');
    myMap.controls
}

