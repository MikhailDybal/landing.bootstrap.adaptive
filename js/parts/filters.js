"use strict";

var closeFiltersPanel = function (e) {
    $(".filter-wrap").css('display','none');
    $('.overlay').slideToggle('fast', function() {
        $('.overlay').css('display','none');
    });
    e.stopPropagation();
    $("html,body").css("overflow","auto");
};

$(document).ready(function(e){
    /* click filters toogle*/
    $("main .main-content .section>.section-header>.filters-wrap>.filter-button-wrap>a").on("click ", function(e){
        $(".filter-wrap").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','block');
            }
        });
        $(".overlay").slideToggle('fast', function() {
            $('.overlay').css('display','block');
        });
        e.stopPropagation();
        $("html,body").css("overflow","hidden");
        //   $("main .filter-wrap").css("overflow","auto");

    });
    $(".filter-wrap>.filter-header .button-close").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main  .overlay").on("click touchstart", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .reset-filters").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .set-filters").on("click", function(e){
        closeFiltersPanel(e);
    });
});


