"use strict";
sendMessage();

$('.contacts-content .contact').on("click", function(e) {
    $('.contacts-content .contact').removeClass("active");
    $(this).removeClass("new-message");
    $(this).find(".new-message-quantity").hide();
    $(this).addClass("active");
});

$('.posters .poster').on("click", function() {
    $('.posters .poster').removeClass("active");
    $(this).addClass("active");
});

$('.posters-header').on("click", function() {
    $('.posters').toggleClass('active');
});
$('.contacts .navbar').on("click", function() {
    $('.contacts-content').toggleClass('active');
});
$('.framework7-root .navbar').on("click", function() {
    $('.messages-content').toggleClass('active');
    $('.messagebar').toggleClass('active');
});

function sendMessage() {
    $('.messagebar .link').on('click', function () {
        // Message text
        var messageText = $('.toolbar-inner textarea').val().trim();
        var myMessagebar = $('.toolbar-inner textarea');
        // Exit if empy message
        if (messageText.length === 0) return;

        // Empty messagebar
        myMessagebar.val("");

        $('.messages').append('<div class="message message-sent"><div class="message-text">' + messageText + '</div> </div>');
        var heightMessageContent = $(".messages").height();
        $('.messages-content').scrollTop(+heightMessageContent);


        //$('.messages-content').scroll
    });
}